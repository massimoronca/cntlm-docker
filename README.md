# Cntlm Docker 

A minimal (<200KB) Docker image for the Cntlm authentication proxy

## Example output

```
$ docker run massimo/cntlm

CNTLM - Accelerating NTLM Authentication Proxy version 0.92.3
Copyright (c) 2oo7-2o1o David Kubicek

This program comes with NO WARRANTY, to the extent permitted by law. You
may redistribute copies of it under the terms of the GNU GPL Version 2 or
newer. For more information about these matters, see the file LICENSE.
For copyright holders of included encryption routines see headers.

------------------------------------------------------------------------
 WARNING  WARNING  WARNING  WARNING  WARNING  WARNING  WARNING  WARNING
------------------------------------------------------------------------
            THIS IS A BASE IMAGE AND CANNOT BE RUN DIRECTLY
------------------------------------------------------------------------

Use the following template to create your own Docker image

  # cntlm.conf
  Username   username
  Domain     DOMAIN
  PassNTLMv2 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  Password   password
  Proxy      proxyserver:80
  NoProxy    localhost,127.0.0.*,10.*,192.168.*
  Listen     3128

  # Dockerfile
  FROM massimo/cntlm
  ADD cntlm.conf /
  CMD ["/cntlm", "-c", "/cntlm.conf", "-f"]

For more examples and ideas, visit:  
https://hub.docker.com/r/massimo/cntlm/

```

```
$ docker images massimo/cntlm
REPOSITORY        TAG        IMAGE ID        SIZE
massimo/cntlm     latest     674f168f78fe    188 kB
```

## Usage

This is a base image, not intended for direct use.

The simplest way to use this image is to creat a full fledged `cntlm.conf` file([instructions here](https://stackoverflow.com/questions/9181637/how-to-fill-proxy-information-in-cntlm-config-file)), add it to the container and then run it.  
Like this

```
  # cntlm.conf
  Username   username
  Domain     DOMAIN
  PassNTLMv2 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  Password   password
  Proxy      proxyserver:80
  NoProxy    localhost,127.0.0.*,10.*,192.168.*
  Listen     3128

  # Dockerfile
  FROM massimo/cntlm
  ADD cntlm.conf /
  CMD ["/cntlm", "-c", "/cntlm.conf", "-f"]
```

For added security you should avoid passing around the passowrd(s).   
Think about committing the `Dockerfile` and related resources to a `git` repository.

In those situations you can pass extra parameters from the env, with only one caveat: the parent proxy param (the `Proxy` directive in the config file) cannot be passed as an argument so you'll always need a `cntlm.conf` file.  

For example

```
  # cntlm.conf
  Username   username
  Domain     DOMAIN
  Proxy      proxyserver:80
  NoProxy    localhost,127.0.0.*,10.*,192.168.*
  Listen     3128

  # Dockerfile
  FROM massimo/cntlm
  ADD cntlm.conf /
  CMD ["/cntlm", "-c", "/cntlm.conf", "-p", "${CNTLM_PASSWORD}" "-f"]
```

And then build and run it with


```
$ docker build -t cntlm .
$ docker run -e CNTLM_PASSWORD=password cntlm
```

Cntlm provides a number of command line params to configure its behaviour

```
  -A  <address>[/<net>]
      ACL allow rule. IP or hostname, net must be a number (CIDR notation)
  -a  ntlm | nt | lm
      Authentication type - combined NTLM, just LM, or just NT. Default NTLM.
      It is the most versatile setting and likely to work for you.
  -B  Enable NTLM-to-basic authentication.
  -D  <address>[/<net>]
      ACL deny rule. Syntax same as -A.
  -d  <domain>
      Domain/workgroup can be set separately.
  -G  <pattern>
      User-Agent matching for the trans-isa-scan plugin.
  -g  Gateway mode - listen on all interfaces, not only loopback.
  -I  Prompt for the password interactively.
  -L  [<saddr>:]<lport>:<rhost>:<rport>
      Forwarding/tunneling a la OpenSSH. Same syntax - listen on lport
      and forward all connections through the proxy to rhost:rport.
      Can be used for direct tunneling without corkscrew, etc.
  -l  [<saddr>:]<lport>
      Main listening port for the NTLM proxy.
  -N  "<hostname_wildcard1>[, <hostname_wildcardN>"
      List of URL's to serve direcly as stand-alone proxy (e.g. '*.local')
  -O  [<saddr>:]<lport>
      Enable SOCKS5 proxy on port lport (binding to address saddr)
  -P  <pidfile>
      Create a PID file upon successful start.
  -p  <password>
      Account password. Will not be visible in "ps", /proc, etc.
  -r  "HeaderName: value"
      Add a header substitution. All such headers will be added/replaced
      in the client's requests.
  -S  <size_in_kb>
      Enable automation of GFI WebMonitor ISA scanner for files < size_in_kb.
  -s  Do not use threads, serialize all requests - for debugging only.
  -U  <uid>
      Run as uid. It is an important security measure not to run as root.
  -u  <user>[@<domain]
      Domain/workgroup can be set separately.
  -w  <workstation>
      Some proxies require correct NetBIOS hostname.
```

A bare minimum configuration looks like this


```
  # cntlm.conf
  Proxy      proxyserver:80

  # Dockerfile
  FROM massimo/cntlm
  ADD cntlm.conf /
  CMD ["/cntlm", \
    "-c", "/cntlm.conf", \
    "-d", "${CNTLM_DOMAIN}" \
    "-u", "${CNTLM_USERNAME}" \
    "-p", "${CNTLM_PASSWORD}" \
    "-N", "${CNTLM_NO_PROXY}" \
    "-f"]

  # run it
  $ docker run -e CNTLM_DOMAIN=domain -e CNTLM_USERNAME=username \
    -e CNTLM_PASSWORD=password -e CNTLM_NO_PROXY='loaclhost, 127.0.0.1' \
    cntlm

```

or invoking Cntlm directly


```
  # cntlm.conf
  Proxy      proxyserver:80

  # Dockerfile
  FROM massimo/cntlm
  ADD cntlm.conf /
  ENTRYPOINT ["/cntlm", "-h"]

  # run it
  docker run cntlm /cntlm -c /cntlm.conf -d domain -u username -p password \
    -N 'localhost, 127.0.0.1' -f
```

Keep in mind that to keep the image size as small as possible, this image is built `FROM scratch` so none of the tools you regularly use in a shell are available, not even `echo` or `cat`.  

That's why we need to create a configration file and add it to an image or pass arguments on the command line, things like `echo "some stuff" > /etc/conf` are not possible.